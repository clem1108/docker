# TP 2 Docker compose et Docker swarm

## 1. Docker compose

### Dockerhub

Je crée mon compte sur `DockerHub`. Je tag mon image, puis je la push sur le repo créé, ce qui donne : 

<img src="pics/dockerhub.png"/>

### Un premier docker compose

Voici le `docker-compose` pour cette partie :
```yml
# choix de la version
version: "3"
# créations des containers
services:

  # création db
  db:
    image: tp1_db
    container_name: mariadb-container
    ports:
      - "3306:3306"
    networks:
      - mynetwork
    volumes:
      - C:\Users\cleme\Documents\ydays\Labo-Cloud-2020-2021\ressources\docker\tp\db:/var/lib/mysql/

  # création node
  node:
    image: clem1108/tp1
    ports:
      - "80:3000"
    networks:
      - mynetwork
    depends_on: 
        - db

# configuration du réseau commun
networks: 
    mynetwork:
```

<a href="Dockerfiles/docker-compose_tp1.yml">Docker-compose</a>

Ce fichier crée les deux containers nécéssaires pour cette partie et les configure en fonction des différents paramètres renseignés.

Vérifions si les deux containers communiquent : 

<img src=pics/connexion.png>

Ils communiquent donc bien.

### Un cas pratique

```yml
version: '3'
services:

  db:
    image: mysql:5.7
    container_name: db
    restart: always
    ports:
        - 3306:3306
    networks: 
        - mynetwork
    environment:
      MYSQL_DATABASE: wordpress
      MYSQL_USER: wordpress
      MYSQL_PASSWORD: wordpress
      MYSQL_ROOT_PASSWORD: toor
    volumes:
      - ./db:/var/lib/mysql

  phpmyadmin:
    image: phpmyadmin
    container_name: phpmyadmin
    restart: always
    ports:
      - 81:80
    networks: 
        - mynetwork
    environment:
      PMA_HOST: db
    links:
      - db:mysql
    volumes:
      - ./phpmyadmin:/etc/phpmyadmin
    depends_on: 
        - db
  
  wordpress:
    image: wordpress
    container_name: wordpress
    restart: always
    ports:
      - 80:80
    networks: 
        - mynetwork
    environment:
      WORDPRESS_DB_HOST: db
      WORDPRESS_DB: wordpress
      WORDPRESS_DB_USER: wordpress
      WORDPRESS_DB_PASSWORD: wordpress
    links:
      - db:mysql
    volumes:
      - ./wordpress:/var/www/html
    depends_on: 
        - db
    

networks: 
    mynetwork:
```

<a href="Dockerfiles/docker-compose.yml">Docker-compose</a>

Ce `docker-compose` crée les trois containers et les configurent sur un réseau commun. Les différentes variables sont aussi renseignées afin que les machines communiquent.

* Wordpress :

<img src=pics/wordpress.png>
Après configuration du site, le site est donc bien fonctionnel

* PHPmyadmin :

<img src=pics/phpmyadmin.png>

Le site est donc bien fonctionnel.

* Captures d'écran des fichiers : 

Les fichiers de `PHPmyadmin` :

<img src=pics/phpmyadmin_files.png>

Les fichiers de `Wordpress` : 

<img src=pics/wordpress_files.png>

Les fichiers de `MySQL`: 

<img src=pics/db_files.png>

## 2. Docker swarm

Après créations des différents noeuds, affichons les : 

```bash
[clement@m1 ~]$ sudo !!
sudo docker node ls
ID                            HOSTNAME            STATUS              AVAILABILITY        MANAGER STATUS      ENGINE VERSION
hhd3r82grmpfqz3y991fhdke1 *   m1.ydays            Ready               Active              Leader              19.03.13
t7awfwq09obq1yy4wyg9tdkyf     m2.ydays            Ready               Active                                  19.03.13
pd8zfdqvznmckucbxitf55rr1     m3.ydays            Ready               Active              Reachable           19.03.13
```
Les trois machines sont donc bien connectées entre-elles sur une `stack` commune.

### Une première application orchestrée : 

Déployons d'abord l'application : 

```bash
[clement@m1 stack]$ docker stack deploy -c docker-compose.yml test
Creating network test_default
Creating service test_web
Creating service test_redis
[clement@m1 stack]$ docker service ls
ID                  NAME                MODE                REPLICAS            IMAGE                   PORTS
q66q15rdfl40        test_redis          replicated          0/1                 redis:alpine
qx5bq97o24r5        test_web            replicated          1/1                 clem1108/stack:latest   *:8080->5000/tcp
```

Maintenant que l'application est déployée, faisons la commande `curl` sur les deux autres machines :

D'abord sur `m2` :
```bash
[clement@m2 ~]$ curl 10.2.1.2:8080
tout en 1 ficher on m'a vu 2 fois.
```
Puis sur `m3` :
```bash
[clement@m3 ~]$ curl 10.2.1.2:8080
tout en 1 ficher on m'a vu 3 fois.
```

Le `serveur` est donc bien accessible.

Listons les stacks :

```bash
[clement@m1 stack]$ docker stack ls
NAME                SERVICES            ORCHESTRATOR
test                2                   Swarm
```

il y en a donc une avec deux services de lançés.

Affichons donc les deux services :

```bash
[clement@m1 stack]$ docker stack services test
ID                  NAME                MODE                REPLICAS            IMAGE                   PORTS
q66q15rdfl40        test_redis          replicated          1/1                 redis:alpine
qx5bq97o24r5        test_web            replicated          1/1                 clem1108/stack:latest   *:8080->5000/tcp
```

Augmentons le nombre de services web :
```bash
[clement@m1 stack]$ docker service scale test_web=3
test_web scaled to 3
overall progress: 3 out of 3 tasks
1/3: running   [==================================================>]
2/3: running   [==================================================>]
3/3: running   [==================================================>]
verify: Service converged
[clement@m1 stack]$ docker stack services test
ID                  NAME                 MODE                REPLICAS            IMAGE                   PORTS
kidt5zznafn0        test_web             replicated          3/3                 clem1108/stack:latest
m4zijmibai1x        test_redis           replicated          1/1                 redis:alpine
```

Nous pouvons donc voir que le service est maintenant répliqué 3 fois.

### Centraliser l'accès au service:

Le service a été créé à partir d'un `docker-compose` différent de celui utilisé précédemment.

Vérifions son accès en tapant `10.2.1.2:8080/dashbord` dans le navigateur :
<img src="pics/traefik.png">

Le site est donc bien fonctionnel.

Maintenant, nous allons lier les deux services. Pour cela, voici le `Docker-compose` :

```yml 
[clement@m1 stack]$ cat docker-compose.yml
version: '3'
services:
  reverse-proxy:
    image: traefik:v2.1.3
    command:
      - "--providers.docker.endpoint=unix:///var/run/docker.sock"
      - "--providers.docker.swarmMode=true"
      - "--providers.docker.exposedbydefault=false"
      - "--providers.docker.network=traefik"
      - "--entrypoints.web.address=:80"
      - "--api.dashboard=true"
      - "--api.insecure=true"
    ports:
      - 8080:8080
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock:ro
    networks:
      - traefik
    deploy:
      placement:
        constraints:
          - node.role == manager
  web:
    image: clem1108/stack:latest
  redis:
    image: "redis:alpine"

    deploy:
      labels:
        - "traefik.enable=true"
        - "traefik.http.routers.webapp.rule=Host(`webapp`)"
        - "traefik.http.routers.webapp.entrypoints=web"
        - "traefik.http.services.webapp.loadbalancer.server.port=3000"
networks:
  traefik:
    external: true
```

Maintenant, vérifions si les deux services sont bien liés :
<img src="pics/traefik_webapp.png">

Le service `Traefik` reconnait bien le service `webapp`. 


<img src="pics/giphy.gif">

