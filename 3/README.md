# TP 3 Ansible
## Le rôle `Common` :
Afin de renseigner à Ansible les différentes machines, il est nécessaire de créer le fichier `inventory.yml` que voici : 
```yml 
all:
  hosts:
    admin.ydays:  
  children:
    worker:
      hosts:
        nodeworker1.ydays: 10.0.0.3
        nodeworker2.ydays: 10.0.0.4
    master:
      hosts:
        nodemaster.ydays: 10.0.0.2
```
<a href="inventory.yml">Inventory</a>

Npus pouvons voir qu'il y a deux groupes distincts : Le groupe worker qui contient les machines `nodeworker1` et `nodeworker2`, et le groupe master qui comporte la machine `nodemaster`.

Testons maintenant le ping : 

```bash
[user@localhost tp]$ ansible worker -m ping
10.0.0.3 | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python3"
    },
    "changed": false,
    "ping": "pong"
}
10.0.0.4 | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python3"
    },
    "changed": false,
    "ping": "pong"
}
[user@localhost tp]$ ansible master -m ping
10.0.0.2 | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python3"
    },
    "changed": false,
    "ping": "pong"
}
```

Le ping est donc fonctionnel.

J'ai créé le fichier `playbook-common.yml` qui est commun aux trois machines : 

```yml
- name: Install some packages
  hosts: master, worker
  tasks:
    - name : install packages
      apt:
        name: apt-transport-https, ca-certificates, curl, gnupg-agent, software-properties-common
        state: present
        update_cache: yes
  become: yes
```
<a href="roles/common/playbook-common.yml">playbook common</a>

Il installe les différents paquets nécéssaires sur les machines.

## Le rôle `Docker` :

Afin d'automatiser l'installation de Docker, j'ai créé le playbook suivant : 
```yml
- name: Install docker
  hosts: master, worker
  tasks:
    - name: Add Docker apt repository key.
      apt_key:
        url: "https://download.docker.com/linux/ubuntu/gpg"
        state: present
    - name: Set the stable docker repository
      apt_repository: 
        repo: "deb [arch=amd64] https://download.docker.com/linux/ubuntu {{ ansible_lsb.codename }} edge"
        state: present
        update_cache: yes
    - name : install docker-ce
      apt:
        name: docker-ce,docker-ce-cli,containerd.io
        state: present
    - name: 
      user: 
        name: ansible
        group: docker
  become: yes
```
<a href="roles/docker/playbook-docker.yml">Playbook Docker</a>

Ce rôle ajoute le repo et les clés nécessaires à l'installation de docker et ensuite installe docker et finit par créer le user `ansible`et l'ajoute au groupe docker. Il s'exécute sur toutes les machines.

## Le rôle `Master` :
Ce rôle s'applique uniquement au `nodemaster` :
```yml 
- name: Kubernetes Master
  hosts: master
  tasks:
    - name: allow 'ansible' to have passwordless sudo
      lineinfile:
        dest: /etc/sudoers
        line: 'ansible ALL=(ALL) NOPASSWD: ALL'
        validate: 'visudo -cf %s'
    - name: disable SWAP
      mount:
        name: "{{ item }}"
        fstype: swap
        state: absent
      with_items:
        - swap
    - name: Disable swap
      command: swapoff -a
      when: ansible_swaptotal_mb > 0
    - name: Add kubernetes apt repository key.
      apt_key:
        url: "https://packages.cloud.google.com/apt/doc/apt-key.gpg"
        state: present
    - name: Set the stable kubernetes repository
      apt_repository:
        repo: "deb https://apt.kubernetes.io/ kubernetes-xenial main"
        state: present
        update_cache: yes
    - name : install kubernetes
      apt:
        name: kubelet, kubeadm, kubectl
        state: present

    - name: Configure node ip
      lineinfile:
        path: '/etc/systemd/system/kubelet.service.d/10-kubeadm.conf'
        line: 'Environment="KUBELET_EXTRA_ARGS=--node-ip=10.0.0.2"'
        regexp: 'KUBELET_EXTRA_ARGS='
        insertafter: '\[Service\]'
        state: present
    - name: Restart kubelet
      service:
        name: kubelet
        daemon_reload: yes
        state: restarted
    - name: Initialize the Kubernetes cluster using kubeadm
      command: kubeadm init --apiserver-advertise-address="10.0.0.2" --apiserver-cert-extra-sans="10.0.0.2"  --node-name="nodemaster" --pod-network-cidr=10.0.0.0/16

    - name: Setup kubeconfig for ansible user
      command: "{{ item }}"
      with_items:
       - mkdir -p /home/ansible/.kube
       - cp -i /etc/kubernetes/admin.conf /home/ansible/.kube/config
       - chown ansible:docker /home/ansible/.kube/config
    - name: Install calico pod network
      become: false
      command: kubectl create -f https://docs.projectcalico.org/v3.4/getting-started/kubernetes/installation/hosted/calico.yaml

    - name: Generate join command
      command: kubeadm token create --print-join-command
      register: join_command

    - name: Copy join command to local file
      local_action: copy content="{{ join_command.stdout_lines[0] }}" dest="./join-command"
  handlers:
    - name: docker status
      service: name=docker state=started
  become: yes
```
<a href="roles/kubernetes_master/playbook-master.yml">Playbook master</a>

Ce rôle autorise ansible à exécuter des commandes en sudo, désactive la SWAP pour le fonctionnement de kubelet, installe kubernetes et ses dépendances, configure le bon fonctionnement de kubernetes et crée le cluster et lance le service docker. 

## Le rôle `Worker` :
Ce rôle est destiné aux `nodeworker1` et `nodeworker2` :
```yml
- name: Kubernetes worker
  hosts: worker
  tasks:
    - name: allow 'ansible' to have passwordless sudo
      lineinfile:
        dest: /etc/sudoers
        line: 'ansible ALL=(ALL) NOPASSWD: ALL'
        validate: 'visudo -cf %s'
    - name: disable SWAP
      mount:
        name: "{{ item }}"
        fstype: swap
        state: absent
      with_items:
        - swap
    - name: Disable swap
      command: swapoff -a
      when: ansible_swaptotal_mb > 0
    - name: Add kubernetes apt repository key.
      apt_key:
        url: "https://packages.cloud.google.com/apt/doc/apt-key.gpg"
        state: present
    - name: Set the stable kubernetes repository
      apt_repository:
        repo: "deb https://apt.kubernetes.io/ kubernetes-xenial main"
        state: present
        update_cache: yes
    - name : install kubernetes
      apt:
        name: kubelet, kubeadm, kubectl
        state: present

    - name: Configure node ip
      lineinfile:
        path: '/etc/systemd/system/kubelet.service.d/10-kubeadm.conf'
        line: 'Environment="KUBELET_EXTRA_ARGS=--node-ip=10.0.0.2"'
        regexp: 'KUBELET_EXTRA_ARGS='
        insertafter: '\[Service\]'
        state: present
    - name: Restart kubelet
      service:
        name: kubelet
        daemon_reload: yes
        state: restarted
    - name: Initialize the Kubernetes cluster using kubeadm
      command: kubeadm init --apiserver-advertise-address="10.0.0.2" --apiserver-cert-extra-sans="10.0.0.2"  --node-name="nodemaster" --pod-network-cidr=10.0.0.0/16

    - name: Setup kubeconfig for ansible user
      command: "{{ item }}"
      with_items:
       - mkdir -p /home/ansible/.kube
       - cp -i /etc/kubernetes/admin.conf /home/ansible/.kube/config
       - chown ansible:docker /home/ansible/.kube/config
    - name: Copy the join command to server location
      copy: src=join-command dest=/tmp/join-command.sh mode=0777

    - name: Join the node to cluster
      command: sh /tmp/join-command.sh


  become: yes
```
<a href="roles/kubernetes_worker/playbook-worker.yml">Playbook worker</a>

Ce rôle autorise ansible à exécuter des commandes en sudo, désactive la SWAP pour le fonctionnement de kubelet, installe kubernetes et ses dépendances, configure le bon fonctionnent de kubernetes et connecte les deux machines aux cluster à l'aide d'un script généré automatiquement.

<img src="pics/giphy.gif">
