# TP 1 : Une introduction à Docker

## I. Prise en main de Docker

### Premier conteneur

Je commence d'abord par récupérer l'image `tutum hello-word` depuis le `hub Docker` : 

```shell
PS C:\WINDOWS\system32> docker pull tutum/hello-world
Using default tag: latest
latest: Pulling from tutum/hello-world
Image docker.io/tutum/hello-world:latest uses outdated schema1 manifest format. Please upgrade to a schema2 image for better future compatibility. More information at https://docs.docker.com/registry/spec/deprecated-schema-v1/
658bc4dc7069: Pull complete                                                                                          a3ed95caeb02: Pull complete                                                                                          af3cc4b92fa1: Pull complete                                                                                          d0034177ece9: Pull complete                                                                                          983d35417974: Pull complete                                                                                          Digest: sha256:0d57def8055178aafb4c7669cbc25ec17f0acdab97cc587f30150802da8f8d85
Status: Downloaded newer image for tutum/hello-world:latest
docker.io/tutum/hello-world:latest
```

Ensuite, je lance le container avec le port 80 : 
```shell
PS C:\Users\cleme> docker run -d -p 80 tutum/hello-world
09c015f2e775a2e43bd3ef0e2ad7686db8126664c644f0ade6d35a157888672f
PS C:\Users\cleme>
PS C:\Users\cleme> docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS
         NAMES
	 09c015f2e775        tutum/hello-world   "/bin/sh -c 'php-fpm…"   10 seconds ago      Up 8 seconds        0.0.0.0:32776->80/tcp   hungry_bohr
```

![](pics/tutum_web.png)

Le service se lance donc correctement.

### Prendre en main les commandes Docker

Affichons les images Docker téléchargées sur mon hôte : 
```shell
PS C:\Users\cleme> docker image ls
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
tutum/hello-world   latest              31e17b0746e4        4 years ago         17.8MB
```
Il y a donc l'image `tutum/hello-world` sur ma machine.

Affichons ensuite la liste des conteneurs en cours de fonctionnement :
```shell
PS C:\Users\cleme> docker container ls
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS
         NAMES
	 6c974353b8ea        tutum/hello-world   "/bin/sh -c 'php-fpm…"   3 minutes ago       Up 3 minutes        0.0.0.0:32778->80/tcp   hello-world
```

Il y a donc le service précédent de lancé.

Affichons ensuite tous les fichiers dans `/www` : 

```bash
/ # ls /www/
index.php  logo.png
```

Il y a donc deux fichiers dans le répertoire racine `/www`.

Stoppons le conteneur : 

```shell
PS C:\Users\cleme> docker container kill hello-world
hello-world
```

Le conteneur s'est donc bien arrêté.

Affichons tous les containers créés : 

```shell
PS C:\Users\cleme> docker container ls -a
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS                            PORTS               NAMES
6c974353b8ea        tutum/hello-world   "/bin/sh -c 'php-fpm…"   9 minutes ago       Exited (137) About a minute ago                       hello-world
```

Il y a donc le container précédent qui, cette fois-ci, est arrêté.

Supprimons ensuite le container :

```shell
PS C:\Users\cleme> docker container prune
WARNING! This will remove all stopped containers.
Are you sure you want to continue? [y/N] y
Deleted Containers:
6c974353b8eac402fb6cda8ce871a8263ab40e459112f3ed573868879301be50

Total reclaimed space: 126B
```

Le container a donc bien été effacé.

## II. Prise en main du DockerFile

Pour build l'image de `nodejs`, j'ai utilisé ce `Dockerfile` : 

```bash
user@MSI:/mnt/c/Users/cleme/Documents/ydays/Labo-Cloud-2020-2021/ressources/docker/tp/app$ cat Dockerfile
# choix de la base
FROM node:14.7-alpine3.12

# choix du port
EXPOSE 3000

# choix du dossier
WORKDIR /app

# copie des fichiers
COPY . /app

# commande npm
RUN npm install

# lancer programme
ENTRYPOINT ["npm"]

# argument
CMD ["start"]
``` 

<a href="Dockerfiles/Dockerfile_app"> DockerFile node</a>

Je build ensuite l'image et je crée le container à l'aide de ces commandes :
Pour donner un `tag` à une image, il suffit d'ajouter l'option `-t` et de donner le nom du `tag`.

```shell
PS C:\Users\cleme\Documents\ydays\Labo-Cloud-2020-2021\ressources\docker\tp\app> docker image build -t tp1 .
[+] Building 0.1s (9/9) FINISHED
=> [internal] load .dockerignore                                                                                  0.0s
=> => transferring context: 2B                                                                                    0.0s
=> [internal] load build definition from Dockerfile                                                               0.0s
=> => transferring dockerfile: 32B                                                                                0.0s
=> [internal] load metadata for docker.io/library/node:14.7-alpine3.12                                            0.0s
=> [1/4] FROM docker.io/library/node:14.7-alpine3.12                                                              0.0s
=> [internal] load build context                                                                                  0.0s
=> => transferring context: 636B                                                                                  0.0s
=> CACHED [2/4] WORKDIR /app                                                                                      0.0s
=> CACHED [3/4] COPY . /app                                                                                       0.0s
=> CACHED [4/4] RUN npm install                                                                                   0.0s
=> exporting to image                                                                                             0.0s
=> => exporting layers                                                                                            0.0s
=> => writing image sha256:9b4e61dcb2c692e0b0734e8c28728a82993fb7c6166658b3366674873ad03985                       0.0s
=> => naming to docker.io/library/tp1
PS C:\Users\cleme> docker run -d -p 80:3000 tp1
e9f0895f963175553f999b3cb51cdb86944856dadf92cc75b1e99cca85aa78c9
```

![](pics/build_web.png)

Comme la page par défaut n'est pas la bonne, si je choisis la page `localhost/connection`, nous pouvons constater que le service est bien fonctionnel.

## III. Communication et gestion de volumes


J'ai créé mes deux `Dockerfile` : 

``` bash 
# choix de la base
FROM node:14.7-alpine3.12

# choix du port
EXPOSE 3000

# choix du dossier
WORKDIR /app

# copie des fichiers
COPY . /app

# commande npm
RUN npm install

# lancer programme
ENTRYPOINT ["npm"]

# argument
CMD ["start"]


user@MSI:/mnt/c/Users/cleme/Documents/ydays/Labo-Cloud-2020-2021/ressources/docker/tp$ cat db/Dockerfile
# choix de la base
FROM mariadb

# choix du port
EXPOSE 3306

RUN echo "#bind-address 127.0.0.1" >> /etc/mysql/my.cnf

ENV MYSQL_USER=user \
MYSQL_PASSWORD=password \
MYSQL_DATABASE=mysqldb \
MYSQL_ROOT_PASSWORD=toor
```

![](pics/mariadb_web_ok.png)

<a href="Dockerfiles/Dockerfile_app"> DockerFile node</a>
<a href="Dockerfiles/Dockerfile_db"> DockerFile mariadb</a>

les commandes à taper sont : 

Je crée d'abord un réseau : 
```shell
docker network create --driver bridge mon-bridge
```

Cela permet de mettre les deux containers dans le même réseau.

Ensuite, je lance les deux containers :

```shell
PS C:\Users\cleme\Documents\ydays\Labo-Cloud-2020-2021\ressources\docker\tp\db> docker run --name mariadb-container -d -v C:\Users\cleme\Documents\ydays\Labo-Cloud-2020-2021\ressources\docker\tp\db:/var/lib/mysql/ -p 3306:3306 --network mon-bridge tp1_db
e21c8a9ab2d1d367e86a0f3a596b305a6c667280fac8100e01b5a8a59e34ceb7
PS C:\Users\cleme\Documents\ydays\Labo-Cloud-2020-2021\ressources\docker\tp\db> docker run -d -p 80:3000 --network mon-bridge tp1
6513d076ba364206d01a72200ca4eb72c1b92e3b278a067d46584f22e5c70d79
```

Les deux containers communiquent donc.

l'ID de connexion est `3`.

Et voilà !

![](pics/giphy.gif)
